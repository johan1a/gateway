def label = "worker-${UUID.randomUUID().toString()}"

podTemplate(label: label,
  serviceAccount: 'jenkins-master',
  slaveConnectTimeout: 600,
  resourceRequestCpu: '500m',
  resourceLimitCpu: '4000m',
  resourceRequestMemory: '1024Mi',
  resourceLimitMemory: '4096Mi',
  containers: [
      containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'kubectl', image: 'lachlanevenson/k8s-kubectl:v1.8.8', command: 'cat', ttyEnabled: true),
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
]) {
  timeout(time: 20, unit: 'MINUTES') {
    node(label) {
      def myRepo = checkout scm
      def gitCommit = myRepo.GIT_COMMIT
      def gitBranch = myRepo.GIT_BRANCH
      def shortGitCommit = "${gitCommit[0..10]}"
      def previousGitCommit = sh(script: "git rev-parse ${gitCommit}~", returnStdout: true)

      stage('Build application and create Docker images') {
        container('docker') {
        withCredentials([string(credentialsId: 'test-host', variable: "DEPLOYMENT_HOST"),
                         string(credentialsId: 'test-user', variable: "DEPLOYMENT_USER"),
                         [$class: 'UsernamePasswordMultiBinding',
                             credentialsId: 'docker-hub-credentials',
                             usernameVariable: 'DOCKER_HUB_USER',
                             passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
            sh """
              docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
              docker build -t johan1a/gateway:${gitCommit} .
              docker build -t johan1a/gateway:latest .
              docker push johan1a/gateway:${gitCommit}
              docker push johan1a/gateway:latest
              """
          }
        }
      }

      stage('Deploy') {
        container('kubectl') {
          sh "kubectl set image deployment gateway gateway=johan1a/gateway:${gitCommit}"
        }
      }
    }
  }
}
