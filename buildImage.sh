#!/bin/sh -e
TAG=$(git rev-parse HEAD)
docker build -t johan1a/gateway:${TAG} .
docker build -t johan1a/gateway:latest .
