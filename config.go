package main

import (
  "fmt"
  "gopkg.in/yaml.v2"
  "io/ioutil"
)

type PathConfig struct {
  LoginUrl          string
  AuthenticationUrl string
  AllowOptionsHost  string
  Auth bool
  Path string
  ServiceName string
  ServicePort int
}


type Config struct {
  ListenPort        int `yaml:"listenPort"`
  LoginUrl          string `yaml:"loginUrl"`
  AuthenticationUrl string `yaml:"authenticationUrl"`
  AllowOptionsHost  string `yaml:"allowOptionsHost"`

  Paths []struct {
    Path string `yaml:"path"`
    Auth bool   `yaml:"auth"`
    ServiceName string `yaml:"serviceName"`
    ServicePort int    `yaml:"servicePort"`
  } `yaml:"paths"`
}

func PathConfigs(config Config) []PathConfig {
  var pathConfigs []PathConfig

  for i := 0; i < len(config.Paths); i++ {
    p := PathConfig {
      LoginUrl: config.LoginUrl,
      AuthenticationUrl: config.AuthenticationUrl,
      AllowOptionsHost: config.AllowOptionsHost,
      Path: config.Paths[i].Path,
      Auth: config.Paths[i].Auth,
      ServiceName: config.Paths[i].ServiceName,
      ServicePort: config.Paths[i].ServicePort,
    }
    pathConfigs = append(pathConfigs, p)
  }
  return pathConfigs
}

func LoadConfig(path string) Config {
  config := Config{}
  data, err := ioutil.ReadFile(path)
  if err != nil {
    fmt.Println(err)
  }
  err = yaml.Unmarshal([]byte(data), &config)
  if err != nil {
    fmt.Println(err)
  }
  return config
}
