package main

import (
  "flag"
  "fmt"
  "log"
  "net/http"
  "net/http/httputil"
  "net/url"
  "strconv"
)

type Prox struct {
  target *url.URL
  proxy  *httputil.ReverseProxy
  PathConfig PathConfig
}

func NewProxy(PathConfig PathConfig) *Prox {
  url, _ := url.Parse(PathConfig.ServiceName)

  return &Prox{target: url,
               proxy: httputil.NewSingleHostReverseProxy(url),
               PathConfig: PathConfig}
}

func (p *Prox) handle(w http.ResponseWriter, r *http.Request) {
  w.Header().Set("Access-Control-Allow-Origin", p.PathConfig.AllowOptionsHost)
  w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")

  p.proxy.Transport = &myTransport{PathConfig: p.PathConfig}
  p.proxy.ServeHTTP(w, r)
}

var port *string

func main() {
  configPath := flag.String("configPath", "./config.yaml", "path to the config file")
  flag.Parse()
  config := LoadConfig(*configPath)
  pathConfigs := PathConfigs(config)

  for i := 0; i < len(pathConfigs); i++ {
    config := pathConfigs[i]
    fmt.Println("Proxying " + config.Path + " to :", config.ServiceName)

    proxy := NewProxy(config)
    http.HandleFunc(config.Path, proxy.handle)
  }
  fmt.Println("Server listening on port:", config.ListenPort)
  log.Fatal(http.ListenAndServe(":" + strconv.Itoa(config.ListenPort), nil))
}
