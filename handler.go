package main

import (
  "bytes"
  "fmt"
  "io/ioutil"
  "net/http"
  "time"
  "strings"
  "net/url"
)

type myTransport struct {
  PathConfig PathConfig
}

var netClient = &http.Client{
  Timeout: time.Second * 30,
}

func respond(code int, message string, request *http.Request) *http.Response {
    body := ""
    response := &http.Response {
      Status:        message,
      StatusCode:    code,
      Proto:         "HTTP/1.1",
      ProtoMajor:    1,
      ProtoMinor:    1,
      Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
      ContentLength: int64(len(body)),
      Request:       request,
      Header:        make(http.Header, 0),
    }
    return response
}

func Unauthorized(request *http.Request) *http.Response {
    return respond(401, "401 unauthorized", request)
}

func OK(request *http.Request) *http.Response {
    return respond(200, "200 OK", request)
}

func EmptyBody() *strings.Reader {
  form := url.Values{}
  return strings.NewReader(form.Encode())
}

func (t *myTransport) RoundTrip(request *http.Request) (*http.Response, error) {

  if request.Method == "OPTIONS" {
    if request.Host == t.PathConfig.AllowOptionsHost || t.PathConfig.AllowOptionsHost == "*" {
      return OK(request), nil
    } else {
      return Unauthorized(request), nil
    }
  }

  if t.PathConfig.Auth {
    var authHeader = request.Header.Get("Authorization")
    fmt.Println("Authorization: " + authHeader)

    req, _ := http.NewRequest("POST", t.PathConfig.AuthenticationUrl, EmptyBody())
    req.Header.Add("Authorization", authHeader)
    authResponse, authError := netClient.Do(req)

    if(authResponse == nil || authError != nil || authResponse.StatusCode != 200){
      fmt.Println("Autherror: ", authError)
      return Unauthorized(request), nil
    }
  }

  return http.DefaultTransport.RoundTrip(request)
}
