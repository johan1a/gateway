#!/usr/bin/env fish
kubectl delete cm gateway-config
kubectl create configmap gateway-config --from-file=config.yaml=config2.yaml
kubectl scale deploy gateway --replicas=0
kubectl scale deploy gateway --replicas=1
